package com.commision.tavsoftpraticaltask.apiClass

import com.commision.tavsoftpraticaltask.model.UserProfileData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("users")
    fun getHospitalsList(@Query("page") page: Int): Call<UserProfileData>
}