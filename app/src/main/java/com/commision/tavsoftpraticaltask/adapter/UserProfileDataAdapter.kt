package com.commision.tavsoftpraticaltask.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.commision.tavsoftpraticaltask.R
import com.commision.tavsoftpraticaltask.model.UserProfileData


class UserProfileDataAdapter(
    private val context: Context,
    val commingOfferList: ArrayList<UserProfileData.Data>
) : RecyclerView.Adapter<UserProfileDataAdapter.InnerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerViewHolder {
        return InnerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_user_profile, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return commingOfferList.size
    }

    override fun onBindViewHolder(holder: InnerViewHolder, position: Int) {
        holder.bind(position, commingOfferList)
        holder.apply {
            val model: UserProfileData.Data = commingOfferList[position]

            Glide.with(context)
                .load(model.avatar)
                .into(im_staff_image)

            tv_first_name.text = model.firstName
            tv_last_name.text = model.lastName
            tv_email.text = model.email
        }


    }

    class InnerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val im_staff_image = view.findViewById<ImageView>(R.id.im_profile_image)
        val tv_first_name = view.findViewById<AppCompatTextView>(R.id.tv_first_name)
        val tv_last_name = view.findViewById<AppCompatTextView>(R.id.tv_last_name)
        val tv_email = view.findViewById<AppCompatTextView>(R.id.tv_email)
        fun bind(
            position: Int,
            treatmentName: ArrayList<UserProfileData.Data>
        ) {
        }
    }
}
