package com.commision.tavsoftpraticaltask.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.commision.tavsoftpraticaltask.R
import com.commision.tavsoftpraticaltask.`interface`.HandleClick
import java.util.*


class DynamicListAdapter(
    private val context: Context,
    val commingOfferList: ArrayList<Int>,
    var handleClick: HandleClick
) : RecyclerView.Adapter<DynamicListAdapter.InnerViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerViewHolder {
        return InnerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_row_number_value, parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return commingOfferList.size
    }

    override fun onBindViewHolder(holder: InnerViewHolder, position: Int) {
        holder.bind(position, commingOfferList)
        holder.apply {
            cl_click.setOnClickListener {
                handleClick.HandleClickPostion(position)
            }


            val r = Random()
            val i1: Int = r.nextInt(commingOfferList.size - 0 + 1) + 0

            Log.e("TAG", "Number Jounarate is$i1")
            if (position == i1) {
                cl_click.setBackgroundColor(ContextCompat.getColor(context, R.color.red))
            } else {
                cl_click.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
            }
            /* val rnd = Random()
             val color: Int = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
             cl_click.setBackgroundColor(color)*/


        }
    }

    class InnerViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val cl_click = view.findViewById<ConstraintLayout>(R.id.cl_click)

        fun bind(
            position: Int,
            treatmentName: ArrayList<Int>
        ) {
        }
    }
}
