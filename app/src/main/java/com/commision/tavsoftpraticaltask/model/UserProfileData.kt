package com.commision.tavsoftpraticaltask.model


import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep
import java.io.Serializable

@Keep
data class UserProfileData(
    @SerializedName("page")
    val page: Int, // 1
    @SerializedName("per_page")
    val perPage: Int, // 6
    @SerializedName("total")
    val total: Int, // 12
    @SerializedName("total_pages")
    val totalPages: Int, // 2
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("support")
    val support: Support
):Serializable {
    @Keep
    data class Data(
        @SerializedName("id")
        val id: Int, // 1
        @SerializedName("email")
        val email: String, // george.bluth@reqres.in
        @SerializedName("first_name")
        val firstName: String, // George
        @SerializedName("last_name")
        val lastName: String, // Bluth
        @SerializedName("avatar")
        val avatar: String // https://reqres.in/img/faces/1-image.jpg
    ):Serializable

    @Keep
    data class Support(
        @SerializedName("url")
        val url: String, // https://reqres.in/#support-heading
        @SerializedName("text")
        val text: String // To keep ReqRes free, contributions towards server costs are appreciated!
    ):Serializable
}