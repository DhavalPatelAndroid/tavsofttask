package com.commision.tavsoftpraticaltask.activity

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.commision.tavsoftpraticaltask.R
import com.commision.tavsoftpraticaltask.`interface`.HandleClick
import com.commision.tavsoftpraticaltask.adapter.DynamicListAdapter
import kotlinx.android.synthetic.main.activity_first_task.*

class FirstTaskActivity : AppCompatActivity(), HandleClick {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_task)
        btnClick()
    }

    fun btnClick() {
        btn_submit.setOnClickListener {
            if (ed_enter_value.text.toString().isNotEmpty()) {
                val temp = ed_enter_value.text.toString().toInt()
                val newGuess = ArrayList<Int>()
                for (i in 0 until temp) {
                    newGuess.add(i)
                }
                dynamicListCreate(newGuess)
            } else {
                Toast.makeText(this@FirstTaskActivity, "Please Enter Value", Toast.LENGTH_SHORT)
                    .show()
            }
        }


    }

    fun dynamicListCreate(data: ArrayList<Int>) {
        rv_dynamic_list.setHasFixedSize(false)
        rv_dynamic_list.layoutManager = GridLayoutManager(this, 3)
        val adapter = DynamicListAdapter(this, data, this)
        rv_dynamic_list.adapter = adapter
    }

    override fun HandleClickPostion(postion: Int) {
        Toast.makeText(this, "Postion => $postion", Toast.LENGTH_SHORT).show()
    }
}