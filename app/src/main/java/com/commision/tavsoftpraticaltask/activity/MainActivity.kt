package com.commision.tavsoftpraticaltask.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.commision.tavsoftpraticaltask.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initButtonClick()
    }

    fun initButtonClick() {
        btn_task_one.setOnClickListener {
            startActivity(Intent(this@MainActivity, FirstTaskActivity::class.java))
        }

        btn_task_two.setOnClickListener {
            startActivity(Intent(this@MainActivity, SecondTaskActivity::class.java))
        }
    }
}