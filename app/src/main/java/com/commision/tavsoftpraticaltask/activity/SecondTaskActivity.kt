package com.commision.tavsoftpraticaltask.activity

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.commision.tavsoftpraticaltask.R
import com.commision.tavsoftpraticaltask.adapter.UserProfileDataAdapter
import com.commision.tavsoftpraticaltask.apiClass.ApiClient
import com.commision.tavsoftpraticaltask.apiClass.ApiInterface
import com.commision.tavsoftpraticaltask.model.UserProfileData
import kotlinx.android.synthetic.main.activity_second_task.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SecondTaskActivity : AppCompatActivity() {
    private var usrModelList: ArrayList<UserProfileData.Data>? = null
    var page = 0
    var limit: Int = 6
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second_task)
        usrModelList = ArrayList()
        setData(page, limit)
        idPBLoading.visibility = View.VISIBLE
        AddNewPage()
    }

    fun AddNewPage() {
        nv_paganing.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            // on scroll change we are checking when users scroll as bottom.
            if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                // in this method we are incrementing page number,
                // making progress bar visible and calling get data method.
                page++
                idPBLoading.visibility = View.VISIBLE
                setData(page, limit)
            }
        })

      /*  swipe_data.setOnRefreshListener(object : SwipeRefreshLayout.OnRefreshListener {
            override fun onRefresh() {
                setData(page, limit)
                swipe_data.setRefreshing(false)
            }
        })*/
    }

    private fun setData(page: Int, limit: Int) {
        if (page > limit) {
            Toast.makeText(this, "That's all the data..", Toast.LENGTH_SHORT).show();
            idPBLoading.visibility = View.GONE;
        } else {
            var apiInterface: ApiInterface =
                ApiClient().getApiClient()!!.create(ApiInterface::class.java)
            apiInterface.getHospitalsList(page).enqueue(object : Callback<UserProfileData> {
                override fun onResponse(
                    call: Call<UserProfileData>?,
                    response: Response<UserProfileData>?
                ) {
                    val data = response!!.body() as UserProfileData
                    idPBLoading.visibility = View.GONE
                    usrModelList!!.addAll(data.data)
                    setAdapter(usrModelList!!)
                }

                override fun onFailure(call: Call<UserProfileData>?, t: Throwable?) {
                }
            })
        }

    }

    fun setAdapter(dataAdapter: ArrayList<UserProfileData.Data>) {
        rv_set_user_profile.setHasFixedSize(false)
        rv_set_user_profile.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val adapter = UserProfileDataAdapter(this, dataAdapter)
        rv_set_user_profile.adapter = adapter
    }
}